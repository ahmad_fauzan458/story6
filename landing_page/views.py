from django.shortcuts import render
from .forms import StatusForm
from.models import Status
from django.http import HttpResponseRedirect

response = {}
# Create your views here.
def index(request):
	status = Status.objects.all().values().order_by('date').reverse()
	response['status'] = status
	response['StatusForm'] = StatusForm
	return render(request, 'index.html', response)

def add_status(request):
	form = StatusForm(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		status = Status(status=request.POST['status'])
		status.save()
		return HttpResponseRedirect("/../landing_page/")
	response['StatusForm'] = form
	return render(request, "index.html", response)