from django.forms import ModelForm
from django import forms
from .models import Status

class StatusForm(ModelForm):

    status = forms.CharField(max_length=300, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    		'placeholder': 'Apakah ada sesuatu yang terjadi ...?',
    	}
    ))

    class Meta:
        model = Status
        fields = 'status',





