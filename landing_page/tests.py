from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Status
from .forms import StatusForm

# Create your tests here.
class LandingPageUnitTest(TestCase):

    def test_landing_page_url_is_exist(self):
        response = Client().get('/landing_page/')
        self.assertEqual(response.status_code, 200)

    def test_lading_page_using_index_func(self):
        found = resolve('/landing_page/')
        self.assertEqual(found.func, index)

    def test_landing_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Hello, Apa kabar?', html_response)

class StatusModelUnitTest(TestCase):

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(status = 'Dikejar-kejar deadline')
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_model_not_create_invalid_status(self):
        #test melebihi max length
        test = "a" *301

         # Creating a new activity
        new_status = Status.objects.create(status = test)
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

class StatusFormUnitTest(TestCase):
    
    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('placeholder="Apakah ada sesuatu yang terjadi ...?"', form.as_p())

    def test_status_validation_for_blank_items(self):
        form = StatusForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )    
    
    def test_lab5_post_success_and_render_the_result(self):
        test = 'masih dikejar deadline' 
        response_post = Client().post('/landing_page/add_status', {'status':test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/landing_page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab5_post_error_and_render_the_result(self):
        #test melebihi max length
        test = "a" *301
        
        response_post = Client().post('/landing_page/add_status', {'status': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/landing_page/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
