from django.db import models
from django.utils import timezone

class Status(models.Model):
	status = models.CharField(max_length=300)
	date = models.DateTimeField(default = timezone.now)