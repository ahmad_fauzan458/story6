# Repository Story 6 PPW
Fakultas Ilmu Komputer, Universitas Indonesia, Semester Gasal 2018/2019
***


## Maintainer

1. Ahmad Fauzan Amirul Isnain - 1706979152

## Pipeline Status

[![pipeline status](https://gitlab.com/ahmad_fauzan458/story6/badges/master/pipeline.svg)](https://gitlab.com/ahmad_fauzan458/story6/commits/master)

## Code Coverage Status

[![coverage report](https://gitlab.com/ahmad_fauzan458/story6/badges/master/coverage.svg)](https://gitlab.com/ahmad_fauzan458/story6/commits/master)

## Heroku Link

https://ppw-e-fauzan-story6.herokuapp.com/