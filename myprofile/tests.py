from django.test import TestCase
from django.test import Client
from .views import index
from django.urls import resolve
from django.http import HttpRequest

# Create your tests here.
class ProfilePageUnitTest(TestCase):

    def test_profile_page_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lading_page_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_profile_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Ahmad Fauzan Amirul Isnain', html_response)

    def test_profile_page_using_profile_template(self):
    	response = Client().get('/profile/')
    	self.assertTemplateUsed(response, 'profile.html')